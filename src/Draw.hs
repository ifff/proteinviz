module Draw (drawAtoms) where

import Bio.PDB.Structure
import Bio.PDB.Structure.Elements
import Bio.PDB.Structure.Vector

import Codec.Picture (PixelRGBA8(..))
import Codec.Picture.Types (Image, PixelRGBA8)

import Data.List

import Debug.Trace (trace)

import GHC.Float

import Graphics.Rasterific
import Graphics.Rasterific.Texture

-- | The minimum/maximum coordinates of the atoms in the protein.
data Bounds = Bounds
  { minX :: Double
  , minY :: Double
  , minZ :: Double
  , maxX :: Double
  , maxY :: Double
  , maxZ :: Double }
  deriving (Show)

-- | Generates bounds for the given list of atoms.
makeBounds :: [Atom] -> Bounds
makeBounds atoms = Bounds
  { minX = getX . coord $ minimumBy cmpX atoms
  , minY = getY . coord $ minimumBy cmpY atoms
  , minZ = getZ . coord $ minimumBy cmpZ atoms
  , maxX = getX . coord $ maximumBy cmpX atoms
  , maxY = getY . coord $ maximumBy cmpY atoms
  , maxZ = getZ . coord $ maximumBy cmpZ atoms }
  where cmpX = cmpCoord getX
        cmpY = cmpCoord getY
        cmpZ = cmpCoord getZ
        cmpCoord proj a1 a2 =
          let c1 = proj (coord a1)
              c2 = proj (coord a2)
          in if c1 < c2 then LT
             else if c1 > c2 then GT
                  else EQ

drawAtoms :: [Atom] -> Image PixelRGBA8
drawAtoms atoms =
  let bounds = makeBounds atoms
      atoms' = {- map (addPerspective bounds) -} (sortByZLayer atoms)
      black = PixelRGBA8 0 0 0 255
      drawColor = PixelRGBA8 0xa1 0x8b 0xe0 255
      w = 500 -- 4 * (maxX bounds - minX bounds)
      h = 500 -- 2 * (maxY bounds - minY bounds)
  in renderDrawing (round w) (round h) black $ do
    withTexture (uniformTexture drawColor) $ do
      mapM_ (drawAtom w h bounds) atoms'

drawAtom width height bounds atom =
  let (x, y, _) = unpackV3 (coord atom)
      xOffset = 100 -- 0.5 * width
      yOffset = 100 -- 0.5 * height
      location = V2 (double2Float (x + xOffset)) (double2Float (y + yOffset))
      drawColor = colorAtom bounds atom
      radius = 5 * (double2Float $ covalentRadius (element atom))
  in do withTexture (uniformTexture (PixelRGBA8 0 0 0 255)) $ do
          fill $ circle location radius
        withTexture (uniformTexture drawColor) $ do
          fill $ circle location radius

sortByZLayer :: [Atom] -> [Atom]
sortByZLayer = reverse . sortOn (getZ . coord)

-- | Modify the coordinates of the atom so that it's in a 3-dimensional perspective with a vanishing point.
addPerspective :: Bounds -> Atom -> Atom
addPerspective bounds atom = atom { coord = V3 x' y' z }
  where x' = x + (q * (midX - x))
        y' = y + (q * (midY - y))
        x = getX (coord atom)
        y = getY (coord atom)
        z = getZ (coord atom)
        q = (maxZ bounds - z) / (maxZ bounds - minZ bounds)
        midX = (minX bounds + maxX bounds) / 2
        midY = (minY bounds + maxY bounds) / 2

getX, getY, getZ :: V3 Double -> Double
getX = (\(x, _, _) -> x) . unpackV3
getY = (\(_, y, _) -> y) . unpackV3
getZ = (\(_, _, z) -> z) . unpackV3

colorAtom bounds atom = PixelRGBA8 r g b depthFactor
  where (r, g, b) = colorByAtomicNum (atomicNumber $ element atom)
        depthFactor = round $ 255 * ((zmax - z) / (zmax - zmin))
        z = getZ (coord atom)
        zmin = minZ bounds
        zmax = maxZ bounds
        a = 255 -- round $ abs (getZ (coord atom))

colorByAtomicNum  1 = (105, 105, 205) -- hydrogen
colorByAtomicNum  6 = (205, 205, 205) -- carbon
colorByAtomicNum  7 = (45, 20, 240) -- nitrogen
colorByAtomicNum  8 = (220, 20, 20) -- oxygen
colorByAtomicNum 12 = (220, 0, 200) -- magnesium
colorByAtomicNum 15 = (90, 10, 10) -- phosphorus
colorByAtomicNum 16 = (210, 220, 0) -- sulfur
colorByAtomicNum 17 = (0, 220, 10) -- chlorine
colorByAtomicNum 19 = (150, 70, 190) -- potassium
colorByAtomicNum  _ = (255, 255, 255) -- default

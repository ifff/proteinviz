module Structure (getAtoms) where

import Bio.PDB.Structure

import Data.Vector (toList)

getAtoms :: Structure -> [Atom]
getAtoms (Structure models') = atoms'
  where models'' = toList models'
        chains' = concatMap (toList . chains) models'
        residues' = concatMap (toList . residues) chains'
        atoms' = concatMap (toList . atoms) residues'

      
      

module Main where

import Bio.PDB.IO

import Draw (drawAtoms)

import Codec.Picture (writePng)

import Control.Monad (when)

import Structure (getAtoms)

import System.Environment (getArgs)

main :: IO ()
main = do
  args <- getArgs
  -- handle bad arguments
  when (null args) $ do
    putStrLn "Expected path for PDB file; got nothing."
    putStrLn "Exiting."
    return ()
  -- if the arguments are good, draw the image
  let path = head args
  maybeStructure <- parse path
  case maybeStructure of
    Nothing -> putStrLn "There was an error reading the PDB file."
    Just structure ->
      let atoms = getAtoms structure
          img = drawAtoms atoms
      in writePng "out.png" img

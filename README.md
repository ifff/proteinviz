# proteinviz
A *very* simple protein visualization tool. It parses PDB files and outputs a 2D image of the molecule's atoms. The atoms are colored according to their element, shaded according to their relative z-coordinate, and the radius of the atoms is proportional to their covalent radius.
Here's an example:

[!an example protein visualization](example.png)
